from django.shortcuts import render, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import NewProjectForm


def redirect_to_projects(request):
    return redirect("list_projects")


@login_required
def view_project_list(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/list.html", context)


@login_required
def view_project(request, id):
    project = Project.objects.get(id=id)
    tasks = Task.objects.filter(project=id)
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = NewProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = NewProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
