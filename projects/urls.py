from django.urls import path
from projects.views import view_project_list, view_project, create_project


urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", view_project, name="show_project"),
    path("", view_project_list, name="list_projects"),
]
