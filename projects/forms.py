# from django import forms
from projects.models import Project
from django.forms import ModelForm


class NewProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
