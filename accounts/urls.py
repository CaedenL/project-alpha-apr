from django.urls import path
from accounts.views import signin, signout, signup


urlpatterns = [
    path("signup/", signup, name="signup"),
    path("logout/", signout, name="logout"),
    path("login/", signin, name="login"),
]
